public class JavaVariableAndDataType02 {
    public static void main(String[] args) {
    /*
    primitive data types (ดั่งเดิม) =>
        จำนวนเต็ม = byte(8), short(16), int(32), long(64)
        ทศนิยม = float(32), double(64)
        ตัวอักษร = char(8), ASCII, Unicode
        ค่าความจริง = boolean(8) (true or false)
    Reference types =>
        ข้อความ = String
    Syntax
        type variableName = value;
    */
// String
System.out.println();
System.out.println("String :");
    String name = "John";
    System.out.println(name);
System.out.println();
// Int
System.out.println("Int :");
    int myNum = 15;
    System.out.println(myNum);
    // or
    int myNum2;
    myNum2 = 15;
    System.out.println(myNum2);
    // or
    int myNum3 = 15;
    myNum3 = 20;  // myNum is now 20
    System.out.println(myNum3);
System.out.println();
// Combine both text and a variable, use the + character
System.out.println("Combine :");
    System.out.println("Hello " + name);
    String firstName = name;
    String lastName = "Doe";
    String fullName = firstName + lastName;
    System.out.println(fullName);

    int x = 5;
    int y = 6;
    System.out.println(x + y); // Print the value of x + y
System.out.println();
// Declare Many Variables
System.out.println("Declare Many Variables :");
    int a = 5;
    int b = 6;
    int c = 50;
    // or int a = 5, b = 6, c = 50; (in one line)
    // Can be
    // int x, y, z;
    // x = y = z = 50;
    System.out.println(a + b + c);
System.out.println();
    }
}
