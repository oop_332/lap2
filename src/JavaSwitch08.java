import java.util.Scanner;

public class JavaSwitch08 {
    public static void main(String[] args) {
        int num = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Num? = ");
        num = sc.nextInt();
        switch (num) {
            case 1 :
                System.out.println("One");
                break;
            case 2 :
                System.out.println("Two");
                break;
            case 3 :
                System.out.println("three");
                break;
        
            default:
                System.out.println("Unknown");
        }
        sc.close();
    }
}
