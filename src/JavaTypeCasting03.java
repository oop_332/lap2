public class JavaTypeCasting03 {
    public static void main(String[] args){
        double num1 = 20.0;
        float num2; // can't because double is bigger then float
        num2 = (float)num1;

        int myInt = 9;
        double myDouble = myInt; // Automatic casting: int to double

    System.out.println("myInt = "+myInt);      // Outputs 9
    System.out.println("myDouble = "+myDouble);   // Outputs 9.0
    System.out.println("num2 = "+num2);
    }
}
/*
Widening Casting (smaller to larger)
byte -> short -> char -> int -> long -> float -> double
*/