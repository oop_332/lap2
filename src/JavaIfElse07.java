public class JavaIfElse07 {
   public static void main(String[] args) {
    int age = 40;
    boolean gender = true;
    if(age>=40){
        if (gender == true){
        System.out.println("Old");}
    }
    else if (age>=20){
        System.out.println("Working age");
    }
    else{
        System.out.println("Unknown");
    }
    // Can Be
    String Gender;
    Gender =(gender)?"Male":"Female";
    System.out.println(Gender);
   } 
   
}
