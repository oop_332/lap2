import java.util.Scanner;

public class JavaString05 {
    public static void main(String[] args) {
        String str = "Hello";
        String str2 = "Hello"; // str กับ str2 มีค่าเท่ากัน
        System.out.println(str.length()); // จำนวน
        System.out.println(str.toLowerCase()); // ทำเป็นตัวเล็ก
        System.out.println(str.toUpperCase()); // ทำตัวใหญ่
        System.out.println(str); // ค่า str จะไม่เปลี่ยนแปลงเหมือน int
        System.out.println(str2);
        /*
         /n new line
         /t tap
        */
        String strNum;
        int num;
        Double dnum;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number : ");
        strNum = sc.next();
        num = Integer.parseInt(strNum); // แปลง String เป็น Int
        dnum = Double.parseDouble(strNum); // แปลง String เป็น Double
        System.out.println("int: "+num);
        System.out.println("Double: "+ dnum);
        System.out.println(String.valueOf(num)); // แปลง Int เป็น String
        System.err.println(""+num); // ต่อค่ากับ String จะเป็น String
        sc.close();
    }
}
