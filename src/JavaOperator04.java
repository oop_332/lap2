public class JavaOperator04 {
    public static void main(String[] args) {
        System.out.println("5/3 = "+5/3); // int ทั้งคู่คำตอบเป็น int
        System.out.println("5.0/3 = "+5.0/3); // int และ float คำตอบเป็น float
        System.out.println("5%3 = "+5%3); // %(mod) หารเอาเศษ
        System.out.println();
        int a = 0;
        a++ ;
        System.out.println("a = "+ a);
        System.out.println("a++ = "+ a++); // print a, a=a+1
        System.out.println("++a = "+ ++a); // a=a+1, print a 
        // * หาทำ * ควรแปลงค่าให้จบก่อน
        
        
    }
    /*
    + คือ บวก
    - คือ ลบ
    * คือ คูณ
    / คือ หาร(เศษ)
    % คือ หาร(ส่วน)
    a++ คือ a = a + 1
    a-- คือ a = a - 1
    a+=10 คือ a = a + 10 (ไม่ออกสอบ)
    && and
    || or
    ! not
    */
}
