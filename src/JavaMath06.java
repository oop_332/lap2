public class JavaMath06 {
    public static void main(String[] args) {
        System.out.println("min (1, 2) = "+Math.min(1, 2)); // จำนวนที่เล็กที่สุด
        System.out.println("max (1, 2) = "+Math.max(1, 2)); // จำนวนที่ใหญ่ที่สุด
        System.out.println("sqrt 16 = "+Math.sqrt(16)); // ถอดรูท
        System.out.println("random number = "+Math.random()); // สุ่มเลข
        System.out.println("round = "+Math.round(10.255)); // แปลงค่ากลับเป็น Int
        System.out.println("abs = "+Math.abs(-4.7)); // ส่งกลับค่าเป็น + (positive)
    }
}
